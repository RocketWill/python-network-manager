# ip-config

## Requirements
```bash
1. Click
2. pyyaml
```

## dhcpcd.conf
1. The file is located at `/etc/dhcpcd.conf`
2. Please make sure the file contains following content with same order.
```
interface eth0
static ip_address=[IP address]/[prefix]
static ip6_address:[IPv6 adress]
static routers=[routers]
static domain_name_servers=[dns]
```

## Usage
### 1. Run the script
```bash
sudo python network_settings.py --configs [/path/to/configs]
```
#### Parameters
1. configs: configuration file including IP, subnet mask, routers, etc.

### 2. Choose an active connection device
The program will list all active connections. Just type the index of the device.  
![choose device](assets/choose_devices.png)

### 3. Proceed
- The program will list the PID. If the IP settings is correct, please kill the process before the program recovers to previous settings.  
![proceed](assets/proceed.png)

- kill process
```bash
cd [PROJECT DIR]
sudo sh kill_ip_setting.sh
```

## Problem records
If active connection devices not display correctly, plesae run below script.
```bash
sudo python restart_network.py
```