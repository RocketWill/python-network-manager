from utils import *
from NetworkManager import *
import ipaddress
import copy
import time
import yaml
import os
import argparse

def run(method, ip_address, subnet_mask, gateway, dns, recover_time):
    '''This command changes ipv4 ip address on Red Hat, Debian, Arch Linux based distros'''
    devices = list(filter(lambda _device: _device.ActiveConnection is not None, NetworkManager.GetAllDevices()))

    inform("List of devices with active connection:")
    for index, device in enumerate(devices):
        click.echo(message=str(index) + ") " + device.Interface)

    choice = click.prompt("Insert the index of target device", type=int)
    assign_ip(method, devices[choice], ip_address, subnet_mask, gateway, dns, recover_time)


def assign_ip(method, device, ip_address, subnet_mask, gateway, dns, recover_time):
    connection = device.ActiveConnection.Connection
    connection_settings = connection.GetSettings()
    old_connection_settings = copy.deepcopy(connection_settings)
    method = method.lower()
    pid = os.getpid()

    inform("Selected device: " + device.Interface)
    inform("Active Connection: " + connection_settings['connection']['id'])
    success("Network configuration to be applied:")
    if method in ["auto", "dhcp"]:
        inform("Method: dhcp")
        inform("Recover time: {}".format(recover_time))
        method = "auto"
    else:
        inform("Method: static")
        inform("IP Address: {}".format(ip_address))
        inform("Subnet Mask: {}".format(subnet_mask))
        inform("Gateway: {}".format(gateway))
        inform("DNS: {}".format(dns))
        inform("Recover time: {}".format(recover_time))
        method = "manual"
    warning("PID: {}".format(pid))
    # warning("If you do not want this program to automatically reset the network settings. Run `sudo kill -9 {}`".format(pid))
    with open("./kill_ip_setting.sh", "w") as text_file:
        text_file.write("kill -9 {pid}".format(pid=pid))

    if click.confirm("Proceed?", abort=True):
        dns_conf = []
        if dns:
            dns_conf = dns.split(',')
        ip_network = ipaddress.IPv4Network((ip_address, subnet_mask), False).compressed
        prefix = int(ip_network.split('/')[1])

        ipv4_config = {"method": "auto"}
        if method == "manual":
            ipv4_config = {
                "method": "manual",
                "addresses": [
                    [ip_address, prefix, gateway]
                ],
                "dns": dns_conf,
                "gateway": gateway,
                # "address-data": [
                #     {
                #         "address": ip_address,
                #         "prefix": prefix
                #     }
                # ]
            }

        connection_settings['ipv4'] = ipv4_config
        device.Disconnect()
        connection.Update(connection_settings)
        NetworkManager.ActivateConnection(connection, device, '/')
        time.sleep(recover_time)
        recover_setting(device, old_connection_settings)

    #print_current_configuration_for(device, connection)

def recover_setting(device, old_connection_settings):
    # NetworkManager.ActivateConnection(old_connection_settings, device, '/')
    # os.system('systemctl restart networking')
    ipv4_config = old_connection_settings['ipv4']
    if "address-data" in ipv4_config:
        del ipv4_config['address-data']
    connection = device.ActiveConnection.Connection
    device.Disconnect()
    old_connection_settings['ipv4'] = ipv4_config
    connection.Update(old_connection_settings)
    NetworkManager.ActivateConnection(connection, device, '/')
    success("Recover settings completed.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Python Network Manager tool')
    parser.add_argument('--configs', default='./configs.yaml', help='Configuration file (.yaml)')
    args = parser.parse_args()

    try:
        with open(args.configs, 'r') as stream:
            configs = (yaml.safe_load(stream))
    except Exception as e:
        error(str(e))
        exit(0)

    method = configs.get('method', None)
    ip_address = configs.get('ip_address', None)
    subnet_mask = configs.get('subnet_mask', None)
    gateway = configs.get('gateway', '0.0.0.0')
    dns = configs.get('dns', None)
    recover_time = float(configs.get('recover_time', 30))

    run(method, ip_address, subnet_mask, gateway, dns, recover_time)
