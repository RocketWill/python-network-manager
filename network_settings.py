#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
import yaml
import time
import os
from utils import *
import argparse
import ipaddress
import subprocess

dhcpdc_file = "/etc/dhcpcd.conf"
# dhcpdc_file = "./dhcpcd.txt"

cur_interface = ""
cur_addr = ""
cur_routers  = ""
cur_dns = ""

def display_info(METHOD, IP, MASK, ROUTERS, DNS):
    if METHOD == "dhcp":
        inform("Method: {}".format(METHOD))
    else:
        inform("Method: {}".format(METHOD))
        inform("IP address: {}".format(IP))
        inform("Subnet Mask: {}".format(MASK))
        inform("Routers: {}".format(ROUTERS))
        inform("DNS: {}".format(DNS))

def get_prefix(IP, MASK):
    ip_network = ipaddress.IPv4Network((IP, MASK), False).compressed
    prefix = int(ip_network.split('/')[1])
    return prefix

def get_current_setting(device):
    global cur_interface, cur_addr, cur_routers, cur_dns
    with open(dhcpdc_file, "r") as file:
        content = file.readlines()
    for i, line in enumerate(content):
        if "interface {}".format(device) in line:
            cur_interface = content[i]
            cur_addr = content[i + 1]
            cur_routers = content[i + 3]
            cur_dns = content[i + 4]
            break

def recover_settings(device):
    global cur_interface, cur_addr, cur_routers, cur_dns
    with open(dhcpdc_file, "r") as file:
        content = file.readlines()
    for i, line in enumerate(content):
        if "interface {}".format(device) in line:
            content[i] = cur_interface
            content[i+1] = cur_addr
            content[i + 3] = cur_routers
            content[i + 4] = cur_dns
            break
    new_content = content
    with open(dhcpdc_file, "w") as file:
        file.writelines(new_content)

def save_ip_changes(device, IP, MASK, ROUTERS, DNS):
    prefix = get_prefix(IP, MASK)
    with open(dhcpdc_file, "r") as file:
        content = file.readlines()
    for i, line in enumerate(content):
        if "interface {}".format(device) in line:
            if(line[0] == "#"):
                content[i] = line[1:]

            addr = content[i + 1]
            routers = content[i + 3]
            dns = content[i + 4]

            [addr, addr_val] = addr.split("=")
            if(addr[0] == "#"):
                content[i+1] = f"{addr[1:]}={IP}/{prefix}\n"
            else:
                content[i+1] = f"{addr}={IP}/{prefix}\n"

            if ROUTERS:
                [router, router_val] = routers.split("=")
                if (routers[0] == "#"):
                    content[i + 3] = f"{router[1:]}={ROUTERS}\n"
                else:
                    content[i + 3] = f"{router}={ROUTERS}\n"

            if DNS:
                [dns, dns_val] = dns.split("=")
                if (routers[0] == "#"):
                    content[i + 4] = f"{dns[1:]}={DNS}\n"
                else:
                    content[i + 4] = f"{dns}={DNS}\n"
            break

    new_content = content
    with open(dhcpdc_file, "w") as file:
        file.writelines(new_content)

def set_dhcp(device):
    with open(dhcpdc_file, "r") as file:
        content = file.readlines()
    for i, line in enumerate(content):
        if "interface {}".format(device) in line:
            if(line[0] != "#"):
                content[i] = "#" + line
            addr = content[i+1]
            routers = content[i+3]
            dns = content[i+4]
            if addr[0] != "#": content[i+1] = "#"+addr
            if routers[0] != "#": content[i + 3] = "#" + routers
            if dns[0] != "#": content[i + 4] = "#" + dns
            break

    new_content = content
    with open(dhcpdc_file, "w") as file:
        file.writelines(new_content)

def restart_network():
    NET_DIR = '/sys/class/net'

    subprocess.call(['sudo', 'systemctl', 'daemon-reload'])
    subprocess.call(['sudo', 'systemctl', 'stop', 'dhcpcd.service'])

    for net_dev in os.listdir(NET_DIR):
        subprocess.call(['sudo', 'ip', 'addr', 'flush', 'dev', net_dev])

    subprocess.call(['sudo', 'systemctl', 'start', 'dhcpcd.service'])
    subprocess.call(['sudo', 'systemctl', 'restart', 'networking.service'])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Python Network Manager tool')
    parser.add_argument('--configs', default='./configs.yaml', help='Configuration file (.yaml)')
    args = parser.parse_args()

    # choose device
    ifs = list(get_local_interfaces().keys())
    inform("List of devices with active connection:")
    for index, device in enumerate(ifs):
        click.echo(message=str(index) + ") " + device)
    choice = click.prompt("Insert the index of target device", type=int)
    device = ifs[choice]
    inform("Selected device: " + device)

    # read nework setting conf
    try:
        with open(args.configs, 'r') as stream:
            configs = (yaml.safe_load(stream))
    except Exception as e:
        error(str(e))
        exit(0)

    method = configs.get('method', None)
    ip_address = configs.get('ip_address', None)
    subnet_mask = configs.get('subnet_mask', None)
    routers = configs.get('routers', '192.168.1.1')
    dns = configs.get('dns', None)
    recover_time = float(configs.get('recover_time', 30))

    get_current_setting(device)
    display_info(method, ip_address, subnet_mask, routers, dns)
    pid = os.getpid()
    warning("PID: {}".format(pid))
    with open("./kill_ip_setting.sh", "w") as text_file:
        text_file.write("kill -9 {pid}".format(pid=pid))

    if click.confirm("Proceed?", abort=True):
        if method == "static":
            save_ip_changes(device, ip_address, subnet_mask, routers, dns)
            restart_network()
            success("Change network setting temporarily, will recover in {} seconds.".format(recover_time))
            time.sleep(recover_time)
            recover_settings(device)
            restart_network()
            success("Recover network settings completed.")
        else:
            set_dhcp(device)
            time.sleep(recover_time)
            recover_settings(device)


