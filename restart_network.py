#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: willc
"""
import subprocess, os

def restart_network():
    NET_DIR = '/sys/class/net'

    subprocess.call(['sudo', 'systemctl', 'daemon-reload'])
    subprocess.call(['sudo', 'systemctl', 'stop', 'dhcpcd.service'])

    for net_dev in os.listdir(NET_DIR):
        subprocess.call(['sudo', 'ip', 'addr', 'flush', 'dev', net_dev])

    subprocess.call(['sudo', 'systemctl', 'start', 'dhcpcd.service'])
    subprocess.call(['sudo', 'systemctl', 'restart', 'networking.service'])

if __name__ == "__main__":
    restart_network()